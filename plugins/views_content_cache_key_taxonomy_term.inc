<?php

/**
 * @file
 * Taxonomy term name plugin for views content cache.
 */

/**
 * Views content cache plugin for taxonomy terms.
 *
 * This plugin allows using taonomy term as an argument to selectively clear
 * Views caches.
 */
class views_content_cache_key_taxonomy_term extends views_content_cache_key {
  function options_form($value, &$handler = NULL) {
    return array(
      '#title' => t('Taxonomy term'),
      '#description' => t('Checks for new or updated nodes wtih a specific taxonomy term (only available through arguments).'),
      '#type' => 'checkboxes',
      '#options' => $this->additional_options_for_arguments($handler->view),
      '#default_value' => $value,
    );
  }

  function content_key($object, $object_type) {
    $keys = array();
    if ($object_type === 'node') {
      $fields = field_read_instances(array('bundle' => $object->type));
      foreach ($fields as $field) {
        $field_info = field_info_field($field['field_name']);
        $terms = field_get_items('node', $object, $field['field_name']);
        if (is_array($terms) && count($terms) > 0) {
          foreach ($terms as $term) {
            if ($field_info['type'] == 'taxonomy_term_reference') {
              $keys[$term['tid']] = $term['tid'];
            }
            // Support for Entity Reference fields that target taxonomy terms.
            elseif ($field_info['type'] == 'entityreference' &&   $field_info['settings']['target_type'] == 'taxonomy_term') {
              $keys[] = $term['target_id'];
            }
          }
        }
      }
    }
    return $keys;
  }

  function view_key($values, &$handler) {
    // We will never allow a user to pick each term in the Views UI, so
    // arguments are the only method to generate this view key.
    return array_unique($this->view_key_replace_arguments($values, $handler));
  }

  function view_key_from_arguments() {
    // For now, we will only accept term IDs.
    return array('views_handler_argument_numeric');
  }
}
